﻿// -----------------------------------------------------------------------
// <copyright file="LaneSelector.cs" company="Tieto">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace racing_bot
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Default lane selector, just picks a lane from list of optimal lanes
    /// Listens OnTrackSegmentEntered event
    /// </summary>
    public class LaneSelector
    {

        Dictionary<int, int> _preferredLaneOnEndOfSegmentCollection;
        IRaceCarController _carController;

        public LaneSelector(IRaceCarController carController)
        {
            _preferredLaneOnEndOfSegmentCollection = new Dictionary<int, int>();
            _carController = carController;
        }

        public void SetPreferredLaneOnTheEndOfSegment(int segmentId, int laneId)
        {
            if (_preferredLaneOnEndOfSegmentCollection.ContainsKey(segmentId))
            {
                _preferredLaneOnEndOfSegmentCollection[segmentId] = laneId;
            }
            else
                _preferredLaneOnEndOfSegmentCollection.Add(segmentId, laneId);
        }

        public void OnSegmentEntered(int currentLane, int currentSegmentId)
        {
            
            if (_preferredLaneOnEndOfSegmentCollection.ContainsKey(currentSegmentId))
            {
                int preferredLane = _preferredLaneOnEndOfSegmentCollection[currentSegmentId];
                if (currentLane != preferredLane)
                {
                    string moveDir = "Left";
                    if (currentLane < preferredLane)
                        moveDir = "Right";

                    _carController.SwitchLane(moveDir);

                }
            }

            
        }
    }
}
