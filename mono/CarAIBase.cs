﻿
namespace racing_bot
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public abstract class CarAIBase
    {
        public IRaceCarController CarController { get; set; }
        private IThrottleCalculator ThrottleCalculator { get; set; }
        public LaneSelector LaneSelector { get; set; }

        private IVelocityLimiter VelocityLimiter { get; set; } 

        public Race Race { get; set; }
        private double CurrentThrottle { get; set; }
        protected CarCollection _carCollection = new CarCollection();
        private string CarName { get; set; }
        private Car _prevCarState;
        private int _lastPiecetId = -1;
        private TurboController _turboController;

        double _lastInPieceDistance = 0;
        private double LastInPieceDistance
        {
            get { return _lastInPieceDistance; }
            set
            {

                _lastInPieceDistance = value;
            }
        }

        protected double Velocity { get; set; }
        
        public CarAIBase(IRaceCarController raceCarController, IThrottleCalculator throttleCalculator, LaneSelector laneSelector, IVelocityLimiter velocityLimiter, TurboController turboCtrl, string carName)
        {
            CarController = raceCarController;
            ThrottleCalculator = throttleCalculator;
            LaneSelector = laneSelector;
            CarName = carName;
            _carCollection.PlayerCarName = carName;
            _turboController = turboCtrl;
            VelocityLimiter = velocityLimiter;
        }

        public void RestartRace()
        {
            for (int i = 0; i < 20; i++ )
                // Console.WriteLine("RACE RESTARTED!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            ThrottleCalculator.RestartRace();
            VelocityLimiter.RestartRace();
        }

        public void UpdateTurboStatus(TurboConfiguration turboConf)
        {
            _turboController.TurboConf = turboConf;
            
        }

        public void UpdateCar(Car car)
        {
            _carCollection.UpdateCar(car);
        }

        public virtual void OnUpdate()
        {
            Car playerCar = _carCollection.GetPlayerCar();
            Piece currentPiece = Race.track.pieces[playerCar.piecePosition.pieceIndex];

            _turboController.Update(currentPiece, Race.track);


            if (_lastPiecetId != playerCar.piecePosition.pieceIndex)
            {
                currentPiece.OnPlayerCarEnter(playerCar, Velocity, VelocityLimiter.LimitedVelocity);
                if (_lastPiecetId >= 0)
                {
                    Piece lastPiece = Race.track.pieces[_lastPiecetId];
                    lastPiece.OnPlayerCarExit(playerCar);
                }
            }

            currentPiece.OnPlayerCarInPiece(playerCar);
                

            CalculateVelocity();
            // Console.WriteLine("piece: " + playerCar.piecePosition.pieceIndex + " | velocity: " + Velocity + " | angle: " + playerCar.angle);
            // TODO: check conditional events, such as segment changed etc.
            CheckConditions();


            double newThrottle = ThrottleCalculator.CalculateThrottle(_carCollection, Race.track.GetPiece(playerCar.piecePosition.pieceIndex), Race, Velocity, Velocity < VelocityLimiter.LimitedVelocity);

            // limit velocity, when for example getting closer to corners
            VelocityLimiter.Calculate(Race, playerCar, Velocity);

            // TODO: refactor to move this out of here
            // calculates if we have passed enough of the corner to go through with full pace
          
            //newThrottle = 1.0f;
            if (Velocity > VelocityLimiter.LimitedVelocity)
            {
                newThrottle = 0.0f;
                Logger.Log("LIMITING VELOCITY++");
            }
            CurrentThrottle = newThrottle;
            CarController.SetThrottle(newThrottle);

            _prevCarState = _carCollection.GetPlayerCar();

            _lastPiecetId = _carCollection.GetPlayerCar().piecePosition.pieceIndex;

            OnUpdateDone();
        }

        protected virtual void OnUpdateDone()
        {

        }
      

        protected virtual void OnRaceStart()
        {

        }


        private void CheckConditions()
        {
            Car playerCar = _carCollection.GetPlayerCar();

            // TODO: has segment changed
            if (_lastPiecetId != playerCar.piecePosition.pieceIndex)
            {
                Piece nextPiece = Race.track.GetPiece(playerCar.piecePosition.pieceIndex + 1);
                
                if (nextPiece.hasSwitch)
                    LaneSelector.OnSegmentEntered(playerCar.piecePosition.lane.startLaneIndex, playerCar.piecePosition.pieceIndex);
            }
        }


        private void CalculateVelocity()
        {
            Car playerCar = _carCollection.GetPlayerCar();

            if (_lastPiecetId >= 0 && _lastPiecetId != playerCar.piecePosition.pieceIndex)
            {
                CalculateTwoPieceVelocity();
            }
            else
            {
                CalculateOnePieceVelocity();
            }
        }

        private void CalculateOnePieceVelocity()
        {
            Car playerCar = _carCollection.GetPlayerCar();
            double inPieceDistance = playerCar.piecePosition.inPieceDistance;

            double velocity = inPieceDistance - LastInPieceDistance;

            if (velocity > 10 || velocity < 0)
            {
                var x = 0;
            }

            LastInPieceDistance = inPieceDistance;

            

//            if (velocity >= 0)
                Velocity = velocity;


        }

        private void CalculateTwoPieceVelocity()
        {
            Car playerCar = _carCollection.GetPlayerCar();
            int currentLane = playerCar.piecePosition.lane.startLaneIndex; // TODO: need to check out if there is case that we need also keep lane history for last piece!
            double inPieceDistance = playerCar.piecePosition.inPieceDistance;

            Piece lastPiece = Race.track.pieces[_lastPiecetId];
            double lastPieceLength = lastPiece.GetLengthOnLane(currentLane);
            double velocity = inPieceDistance + (lastPieceLength - LastInPieceDistance);

            if (velocity > 10 || velocity < 0)
            {
                var x = 0;
            }

            LastInPieceDistance = inPieceDistance;
            
  //          if (velocity >= 0)
                Velocity = velocity;


        }


    }
}
