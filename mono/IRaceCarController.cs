﻿
namespace racing_bot
{
    public interface IRaceCarController
    {
        int GameTick { get; set; }

        void SetThrottle(double throttle);
        void SwitchLane(string direction);
        void ActivateTurbo(string turboText);
    }
}
