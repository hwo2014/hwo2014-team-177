﻿
namespace racing_bot
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Main class that is responsible for CarAI logic
    /// </summary>
    public class CarAI : CarAIBase
    {
        
        public CarAI(IRaceCarController raceCarController, IThrottleCalculator throttleCalculator, LaneSelector laneSelector, IVelocityLimiter velocityLimiter, TurboController turboCtrl, string carName) : base(raceCarController, throttleCalculator, laneSelector, velocityLimiter, turboCtrl, carName)
        {
            
        }


        protected override void OnUpdateDone()
        {

         
        }

        
        
     
       
    }
}
