using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using racing_bot;

public class Bot {

    CarAIBase carAI;

	public static void Main(string[] args) {
		string host = args[0];
		int port = int.Parse(args[1]);
		string botName = args[2];
		string botKey = args[3];

      //  host = "testserver.helloworldopen.com";

		Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        string[] tracks = new string[] { "germany", "germany", "germany", "france", "keimola", "usa", "elaeintarha", "suzuka", "imola", "england" };

        const int numRacesPerTrack = 100;
/*        for (int i = 0; i < numRacesPerTrack; i++)
            foreach (string track in tracks)
        {
            {
                DateTime startTime = DateTime.Now;
                // Console.WriteLine("<-- race with track starts: " + track + " time: "+startTime.ToLongTimeString());
  */              using (TcpClient client = new TcpClient(host, port))
                {
                    NetworkStream stream = client.GetStream();
                    StreamReader reader = new StreamReader(stream);
                    StreamWriter writer = new StreamWriter(stream);
                    writer.AutoFlush = true;

                    new Bot(reader, writer, botName, botKey,"");// track);
                }
    /*            // Console.WriteLine("--> race with track ends: " + track + " time: "+DateTime.Now.ToLongTimeString());


            }
        }*/
	}

	private StreamWriter writer;

	Bot(StreamReader reader, StreamWriter writer, string botName, string botKey, string trackName) 
	{
		this.writer = writer;
		string line;

        if (carAI == null)
            carAI = CreateRaceCar(writer);
        //CarAIBase carAI = CreateRaceCar(writer);

//        CarAIBase carAI = CreateAnalysisCar(writer);
//        string trackname = "keimola";
//       string trackname = "germany";
        string trackname = trackName;
        //send(new CreateRaceMessage(botName, botKey, trackname, "", 1));
        send(new Join(botName, botKey));
    //    send(new JoinRaceMessage(botName, botKey, "keimola", "digg", 3));
       // JoinRaceMessageWrapper joinmsg = new JoinRaceMessageWrapper(botName, botKey, 4);
       // joinmsg.Send(writer);

		while((line = reader.ReadLine()) != null) {
			ReceiveMsgWrapper msg = JsonConvert.DeserializeObject<ReceiveMsgWrapper>(line);
            switch (msg.msgType)
            {
                case "crash":
                    {
                        var x = 0;
                    }break;

                case "turboAvailable":
                    {
                        TurboConfiguration turboConf = (TurboConfiguration)((Newtonsoft.Json.Linq.JToken)(msg.data)).ToObject(typeof(TurboConfiguration));
                        // Console.WriteLine("!------!----------!--------! Turbo is now available");
                        carAI.UpdateTurboStatus(turboConf);
                    }break;
                case "carPositions":
                    {
                        carAI.CarController.GameTick = msg.gameTick;

                        Newtonsoft.Json.Linq.JContainer cont = (Newtonsoft.Json.Linq.JContainer)msg.data;
                        foreach (Newtonsoft.Json.Linq.JToken token in cont)
                        {
                            Car car = (Car)token.ToObject(typeof(Car));
                            carAI.UpdateCar(car);
                        }
                        carAI.OnUpdate();
                        
                    }
                    break;

                case "join":
                    {
                        
                        Console.WriteLine("Joined");

                        send(new Ping());
                    }
                    break;
                case "gameInit":
                    {
                        Console.WriteLine("Race init");
                        carAI.RestartRace();
                        if (carAI.Race == null)
                        {
                            Newtonsoft.Json.Linq.JToken token = (Newtonsoft.Json.Linq.JToken)msg.data;
                            RaceData raceData = (RaceData)token.ToObject(typeof(RaceData));
                            foreach (Piece piece in raceData.race.track.pieces)
                                piece.Race = raceData.race;

                            raceData.race.track.CalculatePieces();
                            raceData.race.track.CalculateThreatLevelForPieces();
                            raceData.race.track.EstimateBestTurboSpot();
                            carAI.Race = raceData.race;

                            PathFinder pathFinder = new PathFinder(raceData.race.track, carAI.LaneSelector);
                            pathFinder.ResolveTrack(0);
                        }
                        //ExportRaceDataCSV(raceData.race, trackName);
                        send(new Ping());
                    }
                    break;
                case "gameEnd":
                    Console.WriteLine("Race session ended");
                    send(new Ping());
                    break;
                case "gameStart":
                    Console.WriteLine("Race session starts");
                    send(new Ping());
                    break;
                default:
                    send(new Ping());
                    break;
            }
		}
	}

    private void ExportRaceDataCSV(Race race, string trackname)
    {
        StreamWriter writer = new StreamWriter(@"C:\temp\"+trackname+".tsv");
        string csvString = "angle;radius;length;est_threat;";
        writer.WriteLine(csvString);
        foreach (Piece p in race.track.pieces)
        {
            csvString = p.angle+";"+p.radius+";"+p.length+";"+p.EstimatedThreat+";";
            writer.WriteLine(csvString);
        }
        writer.Close();

    }

    private CarAIBase CreateRaceCar(StreamWriter writer)
    {
        IGameServerProxy gameServerProxy = new GameServerProxy(writer);
		IRaceCarController raceCarController = new DefaultRaceCarController(gameServerProxy);
		IThrottleCalculator throttleCalculator = new AngleThrottleCalculator();
        TurboController turboController = new TurboController(raceCarController);
    //    IThrottleCalculator throttleCalculator = new AdvancedThrottleCalculator();
		LaneSelector laneSelector = new LaneSelector(raceCarController);
        IVelocityLimiter velocityLimiter = new VelocityLimiter();
 //       IVelocityLimiter velocityLimiter = new ConstantVelocityLimiter(20, 0);
        // TODO: replace preset defaults with optimal line calculator!
/*         laneSelector.SetPreferredLaneOnTheEndOfSegment(2, 1);
        laneSelector.SetPreferredLaneOnTheEndOfSegment(7, 0);
        laneSelector.SetPreferredLaneOnTheEndOfSegment(17, 1);*/
        CarAIBase carAI = new CarAI(raceCarController, throttleCalculator, laneSelector, velocityLimiter, turboController, "Paha Kurki");
        return carAI;
    }

    private CarAIBase CreateAnalysisCar(StreamWriter writer)
    {
        IGameServerProxy gameServerProxy = new GameServerProxy(writer);
		IRaceCarController raceCarController = new DefaultRaceCarController(gameServerProxy);
        IThrottleCalculator throttleCalculator = new ConstantThrottleCalculator(1.0);
		LaneSelector laneSelector = new LaneSelector(raceCarController);
        IVelocityLimiter velocityLimiter = new ConstantVelocityLimiter(6.0, 0.0);
        // TODO: replace preset defaults with optimal line calculator!
        TurboController turboController = new TurboController(raceCarController);
         laneSelector.SetPreferredLaneOnTheEndOfSegment(2, 1);
        laneSelector.SetPreferredLaneOnTheEndOfSegment(7, 0);
        laneSelector.SetPreferredLaneOnTheEndOfSegment(17, 1);
        CarAIBase carAI = new AnalysisCarAI(raceCarController, throttleCalculator, laneSelector, velocityLimiter, turboController, "Paha Kurki");
        return carAI;
    }

	private void send(SendMsg msg) {
        string json = msg.ToJson();
		writer.WriteLine(json);
	}

    

    
}

class JoinRaceMessageWrapper
{
    public string msgType = "joinRace";
    public BotIdData data;

    public JoinRaceMessageWrapper(string botname, string key, int numcars)
    {
        /*if (botname == "paha host")
            msgType = "createRace";*/
        data = new BotIdData();
        data.botId = new BotID(botname, key);
        data.carCount = numcars;
    }

    public void Send(StreamWriter writer)
    {
        string json = JsonConvert.SerializeObject(this);
        writer.WriteLine(json);
    }

   
}

class BotIdData
{
    public BotID botId;
    public int carCount = 3;
   // public string trackname = "imola";
    //public string password = "dikspoks";
}


class SendMsgWrapper {
	public string msgType;
	public object data;
   
	public SendMsgWrapper(string msgType, object data) {
		this.msgType = msgType;
		this.data = data;
	}
}

class ReceiveMsgWrapper {
	public string msgType;
	public object data;
    public int gameTick;
   
	public ReceiveMsgWrapper(string msgType, object data) {
		this.msgType = msgType;
		this.data = data;
	}
}

abstract class SendMsg {
    public string ToJson() {
		return JsonConvert.SerializeObject(new SendMsgWrapper(this.MsgType(), this.MsgData()));
	}
	protected virtual object MsgData() {
		return this;
	}

	protected abstract string MsgType();

    
}

class CreateRaceMessage : SendMsg
{
    public BotID botId;
    public string trackName;
    public string password;
    public int carCount;

    public CreateRaceMessage(string botname, string botkey, string trackname, string password, int numCars)
    {
        this.trackName = trackname;
        this.password = password;
        this.carCount = numCars;
        botId = new BotID(botname, botkey);
    }

    protected override string MsgType() { 
		return "createRace";
	}
}

class JoinRaceMessage : SendMsg
{
    public BotID botId;
    public string trackName;
    public string password;
    public int carCount;

    public JoinRaceMessage(string botname, string botkey, string trackname, string password, int numCars)
    {
        this.trackName = trackName;
        this.password = password;
        this.carCount = numCars;
        botId = new BotID(botname, botkey);
    }

    protected override string MsgType()
    {
        return "joinRace";
    }
}


public class BotID
{
    public BotID(string name, string key)
    {
        this.name = name;
        this.key = key;
    }

    public string name;
    public string key;
}

class Join: SendMsg {
	public string name;
	public string key;
	//public string color;

	public Join(string name, string key) {
		this.name = name;
		this.key = key;
		//this.color = "red";
	}

	protected override string MsgType() { 
		return "join";
	}
}

class Ping: SendMsg {
	protected override string MsgType() {
		return "ping";
	}
}

abstract class JsonMessage
{
    public string ToJson()
    {
        return JsonConvert.SerializeObject(this);
    }

}

// TODO: add gametick values for these messages!
class ThrottleMessage : JsonMessage {
	//public double value;
    public string msgType;
    public double data;
    public int gameTick;


	public ThrottleMessage(double throttle, int tick) {
		this.data = throttle;
        this.gameTick = tick;
        msgType = "throttle";
	}

}

class SwitchLaneMessage: JsonMessage
{
    public string msgType = "switchLane";
    public string data;
    public int gameTick;

    public SwitchLaneMessage(string direction, int tick)
    {
        this.data = direction;
        this.gameTick = tick;
    }
}

class UseTurboMessage : JsonMessage
{
    public string msgType = "turbo";
    public string data;
    
    public UseTurboMessage(string turboText)
    {
        data = turboText;
    }
}