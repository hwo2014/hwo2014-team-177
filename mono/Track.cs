﻿// -----------------------------------------------------------------------
// <copyright file="Track.cs" company="Tieto">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace racing_bot
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class Track
    {
        public string id;
        public string name;
        public Piece[] pieces;
        public LaneSpecification[] lanes;
        public int BestTurboPieceIndex { get; set; }
        public double MinEstimateThreat { get; set; }

        public Piece GetCurrentPiece(Car car)
        {
            return pieces[car.piecePosition.pieceIndex];
        }
        
        public Piece GetNextPiece(Car car)
        {
            int idx = car.piecePosition.pieceIndex + 1;

            if (idx >= pieces.Length)
                idx = 0;

            return pieces[idx];
        }

        /// <summary>
        /// Returns piece with given index, if index is higher than number of pieces, we will start looping from 0
        /// NOTE: we only do this once!
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Piece GetPiece(int index)
        {
            int idx = index;

            if (idx >= pieces.Length)
                idx -= pieces.Length;

            if (idx < 0)
                idx += pieces.Length;

            return pieces[idx];
        }

        /// <summary>
        /// Calculates at least total angles for pieces
        /// </summary>
        public void CalculatePieces()
        {
            for (int i = 0; i < pieces.Length; i++)
            {
                Piece currentPiece = pieces[i];

                if (currentPiece.radius == 0)
                    continue;

                int nextPieceOffset = 1;
                double totalAngle = Math.Abs(currentPiece.angle);

                Piece nextPiece = GetPiece(i+nextPieceOffset);
                while (nextPiece.AngleNormal == currentPiece.AngleNormal && nextPiece.radius == currentPiece.radius)
                {
                    totalAngle += Math.Abs(nextPiece.angle);
                    nextPieceOffset++;
                    nextPiece = GetPiece(i+nextPieceOffset);
                }

                currentPiece.TotalCurveAngle = totalAngle;
            }
        }

        public void CalculateThreatLevelForPieces()
        {
            MinEstimateThreat = 1000.0;
            for (int i = 0; i < pieces.Length; i++)
            {
                Piece currentPiece = pieces[i];
                currentPiece.PieceIndex = i;

                if (currentPiece.IsCornerPiece == false)
                    continue;

                /*if (currentPiece.radius == 0)
                    continue;*/
                /*if (currentPiece.IsCornerPiece == false)
                {
                    currentPiece.EstimatedThreat = 0;
                    continue;
                }*/

                int nextPieceOffset = 1;
                double totalAngle = currentPiece.angle;
            
                double totalDistanceTravelled = currentPiece.GetLengthOnLane(0);

                Piece nextPiece = GetPiece(i + nextPieceOffset);
                double maxAngle = 0;
                double maxThreat = 0;
                //while (nextPiece.AngleNormal == currentPiece.AngleNormal && nextPiece.radius == currentPiece.radius)
                while (totalDistanceTravelled < 250.0f)
                {
                    maxAngle = Math.Max(maxAngle, Math.Abs(totalAngle));
                    
                    double threat = maxAngle / totalDistanceTravelled;
                    maxThreat = Math.Max(maxThreat, threat);

                    totalAngle += nextPiece.angle;
                    totalDistanceTravelled += nextPiece.GetLengthOnLane(0);//nextPiece.length;

                    nextPieceOffset++;
                    nextPiece = GetPiece(i + nextPieceOffset);
                }

                //currentPiece.TotalCurveAngle = totalAngle;
                currentPiece.EstimatedThreat = Math.Sqrt(maxThreat);
                MinEstimateThreat = Math.Min(currentPiece.EstimatedThreat, MinEstimateThreat);
            }
        }

        public void EstimateBestTurboSpot()
        {
            double maxLen = 0;
            
            for (int i = 0; i < pieces.Length; i++)
            {
                int idx = i;
                double maxThreat = GetPiece(idx).EstimatedThreat;
                double totalLen = 0;
                while(maxThreat < 1.0 && idx-i < 8)
                {
                    Piece currPiece = GetPiece(idx);
                    if (idx - i < 5)
                    {
                        totalLen += currPiece.GetLengthOnLane(0);
                        if (currPiece.IsCornerPiece == false)
                            totalLen += currPiece.GetLengthOnLane(0) * 2.0;
                    }
                    maxThreat = Math.Max(maxThreat, currPiece.EstimatedThreat);
                    idx++;
                    
                }

                if (totalLen > maxLen)
                {
                    maxLen = totalLen;
                    BestTurboPieceIndex = i;
                }
            }
        }

        /// <summary>
        /// Go through piece index to re-evaluate threat. This is done when we crash, in which case we want to increase threat for the pieces that were involved
        /// </summary>
        /// <param name="beforePieceIndex"></param>
        public void RecalculateThreat(int beforePieceIndex)
        {}

        public Piece GetFirstPieceOfCorner(int somePieceInCorner)
        {
            Piece currPiece = GetPiece(somePieceInCorner);
            Piece prevPiece = GetPiece(somePieceInCorner-1);
            int offset = 1;
            while ( (prevPiece.IsCornerPiece && prevPiece.AngleNormal == currPiece.AngleNormal) || (currPiece.IsCornerPiece == false))
            {
                offset++;
                currPiece = prevPiece;
                prevPiece = GetPiece(somePieceInCorner - offset);
            }

            return currPiece;
        }
    }
}
