﻿namespace racing_bot
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class ConstantVelocityLimiter : IVelocityLimiter
    {

        double _triggerSpeed;
        double _velocityLimit;
        IVelocityLimiter _mainLimiter;
        bool _triggered = false;
        public ConstantVelocityLimiter(double velocityLimit, double triggerSpeed)
        {
            _mainLimiter = new VelocityLimiter();
            _velocityLimit = velocityLimit;
            _triggerSpeed = triggerSpeed;
            LimitedVelocity = 20.0;
        }

        public void Calculate(Race currentRace, Car playerCar, double currentVelocity)
        {

            if (_triggered)
                return;
               
            if (currentVelocity > _triggerSpeed)
            {
                LimitedVelocity = _velocityLimit;
                _triggered = true;
                return;
            }

            
            _mainLimiter.Calculate(currentRace, playerCar, currentVelocity);

        }

        public double LimitedVelocity
        {
            get
            {
                return _mainLimiter.LimitedVelocity;
            }
            set
            {
                _mainLimiter.LimitedVelocity = value;
            }
        }



        #region IVelocityLimiter Members


        public void RestartRace()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
