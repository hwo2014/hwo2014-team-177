﻿// -----------------------------------------------------------------------
// <copyright file="AdvancedThrottleCalculator.cs" company="Tieto">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace racing_bot
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// NOTE: currently simplified version, we just start slowing down when we notice that we "might" crash and otherwise always throttel
    /// TODO: add evaluation if we already need to slow down or can still move faster before need to slow down
    /// </summary>
    public class AdvancedThrottleCalculator : IThrottleCalculator
    {
        int _carLane;
        double _lastAngle = 0;
        double _maxAngle = 50;

        public double CalculateThrottle(CarCollection carCollection, Piece currentPiece, Race race, double currentVelocity, bool insideSpeedLimit)
        {
            _carLane = carCollection.GetPlayerCar().piecePosition.lane.endLaneIndex;
            Car playerCar = carCollection.GetPlayerCar();
            double currentAngle = playerCar.angle;
            double currentAngleDelta = currentAngle - _lastAngle;
            _lastAngle = currentAngle;
            double positionInPiece = playerCar.piecePosition.inPieceDistance;
            int currentPieceIndex = playerCar.piecePosition.pieceIndex;



            double throttle =  SimulateTicks(currentPiece, race, currentVelocity, currentAngle, currentAngleDelta, positionInPiece, currentPieceIndex);

            Logger.Log("----");
            Logger.Log("throttle: " + throttle);
            Logger.Log("velocity: " + currentVelocity);
            Logger.Log("car angle: " + carCollection.GetPlayerCar().angle);
            Logger.Log("angle delta: " + currentAngleDelta);
//            Logger.Log("angle delta norma: " + angleDeltaNormal);
  //          Logger.Log("angle normal: " + currentPiece.AngleNormal);
            return throttle;

            // TODO: if we went through without crashing, lets try it again with higher speed

            return throttle;
        }

    private double SimulateTicks(Piece currentPiece, Race race, double currentVelocity, double currentAngle, double currentAngleDelta, double positionInPiece, int currentPieceIndex)
    {
            double angleStrength = currentPiece.GetAngleStrength(_carLane) * currentPiece.AngleNormal;
            
            double highestAngle = 0.0;
            // go through 30 ticks with current speed, is there risk for crashing?
            for (int i = 0; i < 100; i++)
            {
                currentAngleDelta = CalculateNextAngleDelta(currentAngleDelta, angleStrength, currentVelocity);

                if (i == 0)
                    Logger.Log("## expected delta: " + currentAngleDelta);
              
                currentAngle += currentAngleDelta;
                highestAngle = Math.Max(highestAngle, Math.Abs(currentAngle));


                if (Math.Abs(currentAngle) > _maxAngle)
                {
                    // risk to crash
                    // TODO: deal with situation
                    // - do we need to start breaking already?
                    // - can we maybe still add extra throttle before braking and survive?
                    return 0.0;
                    break;
                }

                // iterate car movement forward
                positionInPiece += currentVelocity;

                if (positionInPiece >= currentPiece.GetLengthOnLane(_carLane))
                {
                    positionInPiece -= currentPiece.GetLengthOnLane(_carLane);
                    currentPieceIndex++;
                    currentPiece = race.track.GetPiece(currentPieceIndex);
                    angleStrength = currentPiece.GetAngleStrength(_carLane) * currentPiece.AngleNormal;
                }

             
            }
            Logger.Log("expected highest angle: " + highestAngle);
            return 1.0;
    }   

        private double CalculateNextAngleDelta(double currentAngleDelta, double curveStrength, double velocity)
        {
            double x = currentAngleDelta + (velocity * curveStrength) / 50.0;
             double newAngleDelta = 0.02 * Math.Pow(x, 3) + 0.91 * x;
            return newAngleDelta;
        }


        #region IThrottleCalculator Members


        public void RestartRace()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
