﻿
namespace racing_bot
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public interface IThrottleCalculator
    {
        /// <summary>
        /// Throttle is calculated by defaulting to maximum throttle and decreasing by the angular movement (slide)
        /// </summary>
        /// <returns></returns>
        double CalculateThrottle(CarCollection carCollection, Piece currentPiece, Race race, double currentVelocity, bool insideSpeedLimit);
        void RestartRace();

    }
}
