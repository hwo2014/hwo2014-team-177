﻿namespace racing_bot
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Constructs a graph of the lanes. Then goes through lanes, iterating optimal path
    /// </summary>
    public class PathFinder
    {
        Track _track;
        List<int> _optimalLanes = new List<int>();
        LaneSelector _laneSelector;

        public PathFinder(Track track, LaneSelector laneSelector)
        {
            _track = track;
            _laneSelector = laneSelector;
        }

        public void ResolveTrack(int startLane)
        {
            PathNodeInfo nodes = ResolveNode(0, startLane);
            List<int> switches = new List<int>();

            for (int i = 1; i < nodes.LaneList.Count; i++)
            {
                _laneSelector.SetPreferredLaneOnTheEndOfSegment(i-1, nodes.LaneList[i]);
            }

        }

        /// <summary>
        /// Calculates values behind all this combination and returns shortest
        /// </summary>
        /// <param name="piece"></param>
        /// <param name="lane"></param>
        /// <returns></returns>
        public PathNodeInfo ResolveNode(int pieceIndex, int prevLane)
        {
            if (pieceIndex >= _track.pieces.Length)
                return new PathNodeInfo();

            PathNodeInfo bestNodeInfo;
            
            int bestLaneIndex = prevLane;

            double thisLen = _track.pieces[pieceIndex].GetLengthOnLane(prevLane);

            bestNodeInfo = ResolveNode(pieceIndex + 1, prevLane);
            double shortest = bestNodeInfo.PathLength;
            if (_track.pieces[pieceIndex].hasSwitch)
            {
                int nextLane = prevLane+1;
                if (nextLane < _track.lanes.Length)
                {
                    PathNodeInfo info = ResolveNode(pieceIndex + 1, nextLane);

                    if (info.PathLength < shortest)
                    {
                        shortest = info.PathLength;
                        bestNodeInfo = info;
                    }
                }

                nextLane = prevLane-1;
                if (nextLane >= 0)
                {
                    PathNodeInfo info = ResolveNode(pieceIndex + 1, nextLane);

                    if (info.PathLength < shortest)
                    {
                        shortest = info.PathLength;
                        bestNodeInfo = info;
                    }
                }

            }

            bestNodeInfo.LaneList.Insert(0, prevLane);
            bestNodeInfo.PathLength += _track.pieces[pieceIndex].GetLengthOnLane(prevLane);
            
            return bestNodeInfo;
        }
    }

    public class PathNodeInfo
    {
        public List<int> LaneList = new List<int>();
        public double PathLength;
    }
}
