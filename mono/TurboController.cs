﻿
namespace racing_bot
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class TurboController
    {
        public TurboConfiguration TurboConf { get; set; }
        IRaceCarController _carController;

        public TurboController(IRaceCarController carController)
        {
            _carController = carController;
        }

        public void Update(Piece currentPiece, Track track)
        {
            if (TurboConf == null)
                return;


            // TODO: check the right spot to activate turbo
            //if (currentPiece.EstimatedThreat == track.MinEstimateThreat)
            //if (SafeToTurbo(currentPiece, track))
            if (currentPiece.PieceIndex == track.BestTurboPieceIndex)
            {
                ActivateTurbo();
                // Console.WriteLine("BOOOOOOM! TURBO ACTIVATED!");
            }
        }

        private bool SafeToTurbo(Piece currentPiece, Track track)
        {
            // Loop through x - distance of pieces and see if it is okay to turbo
            int pieceIdx = currentPiece.PieceIndex;
            double safelen = 0; // adjust by current car position in first piece
            double totalAng = 0;
            while (safelen < 300.0)
            {
                Piece piece = track.GetPiece(pieceIdx);
                if (Math.Abs(totalAng) > 45)
                    return false;

                totalAng += piece.angle;
                safelen += piece.GetLengthOnLane(0);

                pieceIdx++;
            }

            return true;
        }

        private void ActivateTurbo()
        {
           // // Console.WriteLine("turbo activated");
            _carController.ActivateTurbo("Pröööööt!");
            TurboConf = null;

        }

    }

}
