﻿
namespace racing_bot
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// VelocityLimiter is responsible for limiting maximum velocity of the car depending on conditions.
    /// This VelocityLimiter limits the velocity when we are getting close to curve, depending on the angle of the curve we are to enter 
    /// 
    /// </summary>
    public class VelocityLimiter : racing_bot.IVelocityLimiter
    {
        const double VELOCITY_NOT_LIMITED = 1000.0;
        public double LimitedVelocity { get; set; }

        public double[] _limitingCurve; // list of velocities for given curve strength
        public double[] _breakingDistances;
        
        public double[] _currentMaxAngleForRadius; // max angle received with current radius
        public int[] _piecesPassedAfterLastResetByRadius; // how many times we have gone through pieces like this since we last time changed min/max speed

        int _lastPieceIndex = -1;
        double _lastAngle = 0;
        double _breakingDistanceMultiplier = 1.0;
        double _lastPieceMaxAng = 0;
        public VelocityLimiter()
        {
            _limitingCurve = new double[400];
            _currentMaxAngleForRadius = new double[400];
            _piecesPassedAfterLastResetByRadius = new int[400];

            for (int i = 0; i < _limitingCurve.Length; i++)
            {
                _limitingCurve[i]= -1;
            }

            CalculateInitialLimitingCurveForRadius();
            CalculateInitialBreakingDistances();
        }



        private void CalculateInitialLimitingCurve()
        {
            for (int i = 0; i < 200; i++)
            {
                double nextAngleStrength = ((double)i) / 100.0;
                double targetVelocity = 8.0 - nextAngleStrength;
                _limitingCurve[i] = targetVelocity;
            }

            _limitingCurve[26] = 10.0;
            _limitingCurve[27] = 10.0;
            _limitingCurve[28] = 10.0;
            _limitingCurve[57] = 7.53;
            _limitingCurve[114] = 5.0;
            _limitingCurve[95] = 5.0;
            _limitingCurve[143] = 5.0;
        }

        private void CalculateInitialLimitingCurveForRadius()
        {
            for (int i = 0; i < 400; i++)
            {
                
                /*double nextAngleStrength = ((double)i) / 100.0;
                double targetVelocity = 8.0 - nextAngleStrength;*/
                //double targetVelocity = 9.0 - (i / (200.0 / 3.0));
                double targetVelocity =  9.0 - ((401.0 - i) / 100.0);
                _limitingCurve[i] = targetVelocity;
            }
            /*
            _limitingCurve[26] = 10.0;
            _limitingCurve[27] = 10.0;
            _limitingCurve[28] = 10.0;
            _limitingCurve[57] = 7.53;
            _limitingCurve[114] = 5.0;
            _limitingCurve[95] = 5.0;
            _limitingCurve[143] = 5.0;*/
        }


        /// <summary>
        /// Create graph that describes curve of distance traveled between decrease of the speed
        /// We will use it by taking two points and calculating distance difference between them
        /// </summary>
        private void CalculateInitialBreakingDistances()
        {
            _breakingDistances = new double[2001];

            double lastSpeed = 20;
            double lastMoveTotal = 0;
            _breakingDistances[2000] = 0;

            for (int i = 1999; i > 0; i--)
            {
                _breakingDistances[i] = _breakingDistances[i + 1] + lastSpeed;
                lastSpeed = lastSpeed - lastSpeed / 50.0;
            }
        }
        /*
        private double GetBreakingDistance(double initialSpeed, double targetSpeed)
        {
            int initial = (int)(initialSpeed*100.0);
            int target = (int)(targetSpeed * 100.0);

            return _breakingDistances[target] - _breakingDistances[initial];

        }
        */
        private double GetBreakingDistance(double initialSpeed, double targetSpeed)
        {
            if (targetSpeed >= initialSpeed)
                return 0;
            
            double currentSpeed = initialSpeed;
            double distance = 0;
            while (currentSpeed > targetSpeed)
            {
                distance += currentSpeed;
                currentSpeed -= currentSpeed / 50;
            }

            return distance;
        }

        private double GetLimitFromCurve(double angleStrength)
        {
            
            //int idx = (int)(angleStrength * 100.0);
            int idx = (int)(angleStrength);
            double velocityLimit = _limitingCurve[idx];

            Logger.Log("requested angle index: " + idx);
            Logger.Log("returned velocity limit: " + velocityLimit);

            return velocityLimit;
        }


        /*
        public void CalculateB(Race currentRace, Car playerCar, double currentVelocity)
        {
         
            
            int currentPieceIdx = playerCar.piecePosition.pieceIndex;

            int playerLane = playerCar.piecePosition.lane.endLaneIndex;

            Piece nextPiece = currentRace.track.GetPiece(currentPieceIdx + 1);
            Piece currentPiece = currentRace.track.GetPiece(currentPieceIdx);



            // Is next piece a corner piece?
            if (nextPiece.IsCornerPiece == false)
            {
                LimitedVelocity = VELOCITY_NOT_LIMITED;
                return;
            }

            // What is the angle of the piece we are entering?

            // What is our target velocity for such piece?
            double nextAngleStrength = nextPiece.GetAngleStrength(playerCar.piecePosition.lane.endLaneIndex);
            //double targetVelocity = GetLimitFromCurve(nextAngleStrength);

            double rad = nextPiece.radius;
            double targetVelocity = GetLimitFromCurve(rad);
            
            // What is our current velocity?



            // How long does it take to "break" to get into that velocity?
            //double distanceNeededToBreak = Math.Max(30.0f, 30.0 * (velocityDiff+ angleChange));
            double distanceNeededToBreak = GetBreakingDistance(currentVelocity, targetVelocity);//30.0 * (velocityDiff) - 30;
            Logger.Log("piece index: " + currentPieceIdx);
            Logger.Log("target velocity: " + targetVelocity);
            Logger.Log("next piece radius: " + rad);
            Logger.Log("---- distanceBreak:" + distanceNeededToBreak);
            Logger.Log("piece length: " + currentPiece.GetLengthOnLane(playerCar.piecePosition.lane.endLaneIndex));
            // Are we on region that we have to start "breaking"?
            if (DistanceLeftOnPiece(currentRace, playerCar) <= distanceNeededToBreak)
            {
                // - if so, set the limited velocity to target velocity
                LimitedVelocity = targetVelocity;

            }
        }
        */
        public void Calculate(Race currentRace, Car playerCar, double currentVelocity)
        {
            int currentPieceIdx = playerCar.piecePosition.pieceIndex;

            int playerLane = playerCar.piecePosition.lane.endLaneIndex;
            Piece currentPiece = currentRace.track.GetPiece(currentPieceIdx);
            double distanceToPiece = currentPiece.GetLengthOnLane(playerLane) - playerCar.piecePosition.inPieceDistance;

            // deal with adjusting the speeds
            double rad = currentPiece.radius;

            if (rad > 0)
            {
                _currentMaxAngleForRadius[(int)rad] = Math.Max(_currentMaxAngleForRadius[(int)rad], Math.Abs(playerCar.angle));
            }

            _lastPieceMaxAng = Math.Max(_lastPieceMaxAng, Math.Abs(playerCar.angle));

            // entering new piece
            if (_lastPieceIndex != currentPieceIdx)
            {
                if (_lastPieceMaxAng < 25)
                {
                    Piece lastPiece = currentRace.track.GetPiece(_lastPieceIndex);
                    lastPiece.EstimatedThreat = Math.Max(0, lastPiece.EstimatedThreat - (2.0-(_lastPieceMaxAng / 12.5))*0.3);
                }

                _lastPieceMaxAng = 0;

                if (rad > 0)
                {
                    _piecesPassedAfterLastResetByRadius[(int)rad]++;
                    if (_piecesPassedAfterLastResetByRadius[(int)rad] > 6)
                    {
                        double maxAng = _currentMaxAngleForRadius[(int)rad];

                        if (maxAng <= 50)
                        {
                            double madelta = (1.0 - (maxAng / 55)) / 3.0;
                            _limitingCurve[(int)rad] += madelta; //*= 1.01;
                            // Console.WriteLine("Curve(" + (int)rad + ") speed limit increased to: " + _limitingCurve[(int)rad]);

                            _piecesPassedAfterLastResetByRadius[(int)rad] = 0;
                            _currentMaxAngleForRadius[(int)rad] = 0;


                         
                        }
                    }
                }

                if (LimitedVelocity != VELOCITY_NOT_LIMITED && currentPiece.IsCornerPiece)
                {
                    // did we break to the level we were supposed?
                    if (currentVelocity - LimitedVelocity > 0.05)
                    {
                        _breakingDistanceMultiplier *= 1.01;
                        // Console.WriteLine("Breaking Distance Multiplier increased: " + _breakingDistanceMultiplier);
                        // Console.WriteLine("Current velocity: " + currentVelocity);

                    }
                    else if (currentVelocity - LimitedVelocity < -0.05)
                    {
                        _breakingDistanceMultiplier /= 1.01;
                        // Console.WriteLine("Breaking Distance Multiplier lowered: " + _breakingDistanceMultiplier);
                        // Console.WriteLine("Current velocity: " + currentVelocity);
                    }
                }
            }

            Logger.Log("Estimated threat: " + currentPiece.EstimatedThreat);
            Logger.Log("Balanced threat: " + currentPiece.BalancedThreat);
            if (Math.Abs(playerCar.angle) > 45.0 || (Math.Abs(playerCar.angle) == 0 && Math.Abs(_lastAngle) > 40.0f))
            {
                Piece curvePiece = currentRace.track.GetFirstPieceOfCorner(playerCar.piecePosition.pieceIndex);
        /*        int offs = 0;
                // TODO: try out so that move this to earliest piece of same corner
                while (curvePiece.radius == 0.0)
                {
                    offs--;
                    curvePiece = currentRace.track.GetPiece(playerCar.piecePosition.pieceIndex + offs);
                }
          */      rad = curvePiece.radius;
                  curvePiece.EstimatedThreat += (Math.Abs(playerCar.angle) / 60) * 0.1;
                _limitingCurve[(int)rad] /= 1.01;
                // Console.WriteLine("Curve(" + (int)rad + ") speed limit decreased to: " + _limitingCurve[(int)rad]);


                if (Math.Abs(playerCar.angle) == 0 && Math.Abs(_lastAngle) > 40.0f)
                {
                    // Console.WriteLine("crash");
                    // Console.WriteLine("_-_-__ Added crash to piece: " + curvePiece.PieceIndex);
                    curvePiece.NumCrashesOnPiece++;
                    _limitingCurve[(int)rad] /= 1.02;
                    // Console.WriteLine("Curve(" + (int)rad + ") speed limit decreased to: " + _limitingCurve[(int)rad] + " due to crash") ;

                    

                }
                _piecesPassedAfterLastResetByRadius[(int)rad] = 0;
                _currentMaxAngleForRadius[(int)rad] = 0;
            }

            _lastAngle = playerCar.angle;

            _lastPieceIndex = currentPieceIdx;

            for (int i = 0; i < 8; i++)
            {

                int nextPieceIndex = currentPieceIdx + 1 + i;
                if (nextPieceIndex >= currentRace.track.pieces.Length)
                    nextPieceIndex -= currentRace.track.pieces.Length;

                Piece nextPiece = currentRace.track.GetPiece(nextPieceIndex);

                // Is next piece a corner piece?
                if (nextPiece.IsCornerPiece)
                {


                    // What is our target velocity for such piece?
/*                    double nextAngleStrength = nextPiece.GetAngleStrength(playerLane);

                    double targetVelocity = GetLimitFromCurve(nextAngleStrength);
  */                  // What is our current velocity?
                   rad = nextPiece.radius;
                   double targetVelocity = GetLimitFromCurve(rad) / nextPiece.DangerFactor - Math.Abs(currentPiece.BalancedThreat);// *((20.0 - Math.Sqrt(nextPiece.TotalCurveAngle)) / 10.0);
                   if (targetVelocity < 2)
                       targetVelocity = 2;
                   // Logger.Log("total curve angle: " + nextPiece.TotalCurveAngle);
                    Logger.Log("target velocity: " + targetVelocity);
                    Logger.Log("next piece radius: " + rad);
                    

                    // How long does it take to "break" to get into that velocity?
                    //double distanceNeededToBreak = Math.Max(30.0f, 30.0 * (velocityDiff+ angleChange));
                    double distanceNeededToBreak = GetBreakingDistance(currentVelocity, targetVelocity) * _breakingDistanceMultiplier;//30.0 * (velocityDiff) - 30;

                    if (distanceToPiece <= distanceNeededToBreak)
                    {
                        // - if so, set the limited velocity to target velocity
                        LimitedVelocity = targetVelocity;
                        return;
                    }
                }

                distanceToPiece += nextPiece.GetLengthOnLane(playerLane);
            }

            LimitedVelocity = VELOCITY_NOT_LIMITED;     
        }

        private double DistanceLeftOnPiece(Race race, Car playerCar)
        {
            Piece currentPiece = race.track.GetPiece(playerCar.piecePosition.pieceIndex);
            int currentLane = playerCar.piecePosition.lane.endLaneIndex;
            double pieceLength = currentPiece.GetLengthOnLane(playerCar.piecePosition.lane.endLaneIndex);
            double distanceLeft = pieceLength - playerCar.piecePosition.inPieceDistance; 
            return distanceLeft;
        }



        public void RestartRace()
        {
            this._lastPieceIndex = -1;
            this._lastAngle = 0;
            LimitedVelocity = VELOCITY_NOT_LIMITED;
            this._lastPieceMaxAng = 0;
        }

    }
}
