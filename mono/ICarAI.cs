﻿using System;
namespace racing_bot
{
    public interface ICarAI
    {
        void OnRaceStart();
        void OnUpdate();
        Race Race { get; set; }
        void UpdateCar(Car car);
    }
}
