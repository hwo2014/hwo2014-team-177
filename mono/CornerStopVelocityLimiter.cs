﻿
namespace racing_bot
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class CornerStopVelocityLimiter : IVelocityLimiter
    {
        double _straightLimit; 

        public CornerStopVelocityLimiter(double straightLimit)
        {
            _straightLimit = straightLimit;
        }
       
        public void Calculate(Race currentRace, Car playerCar, double currentVelocity)
        {
            if (currentRace.track.pieces[playerCar.piecePosition.pieceIndex].IsCornerPiece)
            {
                LimitedVelocity = 0.0f;
            }
            else
                LimitedVelocity = _straightLimit;
        }

        public double LimitedVelocity
        {
            get;
            set;
        }



        #region IVelocityLimiter Members


        public void RestartRace()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
