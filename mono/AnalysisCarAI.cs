﻿
namespace racing_bot
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.IO;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class AnalysisCarAI : CarAIBase
    {
        StreamWriter _writer;
        
        public AnalysisCarAI(IRaceCarController raceCarController, IThrottleCalculator throttleCalculator, LaneSelector laneSelector, IVelocityLimiter velocityLimiter, TurboController turboCtrl, string carName) : base(raceCarController, throttleCalculator, laneSelector, velocityLimiter, turboCtrl, carName)
        {
            _writer = new StreamWriter(@"C:\temp\analysis_data.tsv");
            
        }

        

        protected override void OnUpdateDone()
        {

        
            Car playerCar = _carCollection.GetPlayerCar();
            int pieceIndex = playerCar.piecePosition.pieceIndex;
            Piece currentPiece = Race.track.pieces[pieceIndex];

            // TODO: collect metrics and log it as csv (later on switch to use data dynamically!)
            Logger.Log("------");
            Logger.Log("velocity: " + Velocity);
            Logger.Log("car angle: " + playerCar.angle);
            Logger.Log("piece length: " + currentPiece.GetLengthOnLane(playerCar.piecePosition.lane.endLaneIndex));
            Logger.Log("piece angle: " + currentPiece.angle);
            Logger.Log("piece radius: " + currentPiece.radius);
            Logger.Log("piece index: " + pieceIndex);
            Logger.Log("inPiece position: " + playerCar.piecePosition.inPieceDistance);
            string csvString = Velocity + ";" + playerCar.angle + ";" + currentPiece.radius +";"+ currentPiece.GetLengthOnLane(playerCar.piecePosition.lane.endLaneIndex) + ";" + currentPiece.angle + ";" + pieceIndex + ";" + playerCar.piecePosition.inPieceDistance + ";";
            _writer.WriteLine(csvString);
            _writer.Flush();
        }

    }
}
