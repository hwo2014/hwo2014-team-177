﻿
namespace racing_bot
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class DefaultRaceCarController : IRaceCarController
    {
        IGameServerProxy _gameServer;
        public int GameTick { get; set; }
        public DefaultRaceCarController(IGameServerProxy gameServer)
        {
            _gameServer = gameServer;
        }

        public void SetThrottle(double throttle)
        {
            _gameServer.SetThrottle(throttle, GameTick);
        }

        public void SwitchLane(string direction)
        {
            _gameServer.SwitchLane(direction, GameTick);
        }

        public void ActivateTurbo(string turboText)
        {
            _gameServer.ActivateTurbo(turboText, GameTick);
        }
   }
}
