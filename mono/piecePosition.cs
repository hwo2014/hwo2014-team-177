﻿// -----------------------------------------------------------------------
// <copyright file="piecePosition.cs" company="Tieto">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace racing_bot
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class PiecePosition
    {
        public int pieceIndex { get; set; }
        public Lane lane { get; set; }
        public double inPieceDistance { get; set; }
    }
}
