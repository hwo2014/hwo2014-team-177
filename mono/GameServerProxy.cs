﻿
namespace racing_bot
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
using System.IO;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class GameServerProxy : racing_bot.IGameServerProxy
    {
        StreamWriter _streamWriter;
        int lastTickSend = -1;
        public GameServerProxy(StreamWriter writer)
        {
            _streamWriter = writer;
        }

        public void SetThrottle(double throttle, int gameTick)
        {
            SendMessage( new ThrottleMessage(throttle, gameTick), gameTick);
        }
        
        public void SwitchLane(string direction, int gameTick)
        {
            SendMessage(new SwitchLaneMessage(direction, gameTick), gameTick);
        }

        public void ActivateTurbo(string turboText, int gameTick)
        {
            SendMessage(new UseTurboMessage(turboText), gameTick);
        }

        private void SendMessage(JsonMessage msg, int gameTick)
        {
            // skip message if already sent one in this round to not get out of sync
            if (lastTickSend == gameTick)
                return;
            lastTickSend = gameTick;

            string json = msg.ToJson();
    		_streamWriter.WriteLine(json);
        }
    }
}
