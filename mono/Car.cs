﻿
namespace racing_bot
{
    
    /// <summary>
    /// Current state of the car
    /// </summary>
    public class Car
    {
        public float angle { get; set; }
        //public string name { get; set; }
        public CarID id { get; set; }
        public PiecePosition piecePosition { get; set; }

    }

    public class CarID
    {
        public string name { get; set; }
    }
}
