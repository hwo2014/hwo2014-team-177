﻿
namespace racing_bot
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Newtonsoft.Json;
    
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class Piece
    {
        public double length;
        [JsonProperty("switch")]
        public bool hasSwitch = false;
        public double radius;
        public double angle;

        public int PieceIndex { get; set; }
        public double TotalCurveAngle { get; set; } // total angle of whole curve as starting from this piece

        public bool IsCornerPiece { get { return radius != 0.0; } }
        private List<double> _lengthOnLane = new List<double>();

        public Race Race { get; set; }
        int _numCrashesOnPiece;
        public int NumCrashesOnPiece { get { return _numCrashesOnPiece; } set { _numCrashesOnPiece = value;} }
        public double MaxCarAngleInTwoLastPasses { get; set; }

        public double EstimatedThreat { get; set; } 
        public double BalancedThreat { get { return EstimatedThreat* ThreatBalancerDueSuccess; } }
        private double _threatBalancer = -1.0;
        private double ThreatBalancerDueSuccess { get { if (_threatBalancer == -1) return 1.0;  return _threatBalancer; } set { _threatBalancer = value; } } // this is for case when we have estimated threat in some corner high but keep passing it nicely
        private double _playerCarMaxAngle;
        private bool _validToCalculateBalancer;
        public Piece()
        {
            MaxCarAngleInTwoLastPasses = 60.0;
        }

        public void OnPlayerCarEnter(Car playerCar, double velocity, double maxVelocity)
        {
            _validToCalculateBalancer = false;
            if (maxVelocity - velocity < 0.3)
                _validToCalculateBalancer = true;
            
            _playerCarMaxAngle = 0;
        }

        public void OnPlayerCarExit(Car playerCar)
        {
           /* if (_playerCarMaxAngle > 50)
                EstimatedThreat += 0.2;
           
            */
            if (_validToCalculateBalancer == false)
                return;



            /*            if (_playerCarMaxAngle > MaxCarAngleInTwoLastPasses)
                        {
                            MaxCarAngleInTwoLastPasses = _playerCarMaxAngle;
                        }
                        else*/
            { 
                MaxCarAngleInTwoLastPasses = (MaxCarAngleInTwoLastPasses + _playerCarMaxAngle*3) / 4.0;
            }


            ThreatBalancerDueSuccess = MaxCarAngleInTwoLastPasses / 60.0;//(Math.Pow(MaxCarAngleInTwoLastPasses,2) / 3600.0);
        }

        public void OnPlayerCarInPiece(Car playerCar)
        {
                
            _playerCarMaxAngle = Math.Max(Math.Abs(playerCar.angle), _playerCarMaxAngle);
        }


        public double DangerFactor
        {
            get
            {
                return 1.0 + (NumCrashesOnPiece) * 0.25;
//                return 1.0;
            }
        }



        public double AngleNormal
        {
            get
            {
                if (angle == 0)
                    return 0;

                return angle / Math.Abs(angle);
            }
       
        }

       
        public double GetAngleStrength(int laneIndex)
        {
            if (angle == 0)
                return 0;

            double len = GetLengthOnLane(laneIndex);
            return Math.Abs(angle) / len;
        }
        /*
        
        public double GetAngleStrength(int laneIndex)
        {
            if (angle = 0)
            return 0;

           return 
        }*/
        public double GetLengthOnLane(int laneIndex)
        {
//            laneIndex = 1 - laneIndex;

            // if needed, increase number of lanes
            if (laneIndex >= _lengthOnLane.Count)
            {
                int origLaneCount = _lengthOnLane.Count;
                for (int i = 0; i < laneIndex - origLaneCount + 1; i++)
                {
                    _lengthOnLane.Add(0);
                }
            }

            if (_lengthOnLane[laneIndex] == 0)
            {
                if (IsCornerPiece)
                {
                    double laneDistanceFromCentre = Race.track.lanes[laneIndex].distanceFromCenter;
                    // TODO: calculate length by lane (we need lane information for that!)
                    double angleNormalized = angle / Math.Abs(angle);
                    double rad = radius - laneDistanceFromCentre * angleNormalized;
                    _lengthOnLane[laneIndex] = rad * 2 * Math.PI * (Math.Abs(angle) / 360.0);

                }
                else
                {
                    _lengthOnLane[laneIndex] = length;
                }
            }

            return _lengthOnLane[laneIndex];
        }
    }
}
