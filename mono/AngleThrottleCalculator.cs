﻿
namespace racing_bot
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class AngleThrottleCalculator : IThrottleCalculator
    {
        private double lastAngle = 0.0f;
        private double lastAngleDelta = 0.0f;
        private double _throttleLimitMultiplier = 1.0f;
        private int ticksSinceLastHighAngle = 0;

        

        public double CalculateThrottle(CarCollection carCollection, Piece currentPiece, Race race, double currentVelocity, bool insideSpeedLimit)
        {

            Car playerCar = carCollection.GetPlayerCar();

            double angle = carCollection.GetPlayerCar().angle;
            double angleDelta = Math.Abs(lastAngle - angle);
            double angleDeltaNormal = (lastAngle - angle) / angleDelta;

            double throttle = 1.0f;
            
/*
            angleDelta += 0.5;
            angleDelta *= angleDelta;
            */

            // TODO: should we check in which kind of corner we are? are we getting closer or further?
            // So, if (currentpiece->isCurve && currentpiece->curvenormal is same direction as deltachange, we activate)
           /* if (currentPiece.angle != 0)
            if ( (Math.Abs(lastAngle) < Math.Abs(angle)) || (currentPiece.AngleNormal !=  angleDeltaNormal))*/

           // if (currentPiece.angle != 0 && currentPiece.AngleNormal != angleDeltaNormal)

            bool angleBiggerThanOnLastUpdate  =Math.Abs(lastAngle) < Math.Abs(angle);
            double carAngleNormal = angle / Math.Abs(angle);

            double angleDeltaDelta = angleDelta - lastAngleDelta;
            lastAngleDelta = angleDelta;

            Logger.Log("angledeltadelta: " + angleDeltaDelta);

            if (insideSpeedLimit && angleDelta > 40.0)
            {
                _throttleLimitMultiplier *= 1.1f;
                // Console.WriteLine("Throttle Limit Multiplier increased: " + _throttleLimitMultiplier);
            }



            // NOTE: AngleDeltaDelta check slows us currently down, as does limiting speed by looking multiple blocks.. need to figure out if it is good as it is or need fine tunint
            // might also be that with this rule the speed that we can enter to corners can be higher as well!

            if ( (angleDelta> 1.0 || angleDeltaDelta > 0) && currentPiece.angle != 0 && ( angleBiggerThanOnLastUpdate || (carAngleNormal != currentPiece.AngleNormal && angleDelta > 2.0) ))
                throttle = 1.0 / (angleDelta * _throttleLimitMultiplier * currentVelocity +1.0);

            if (Math.Abs(angle) > 0)
            {
                if (Math.Abs(angle) < 50.0)
                    ticksSinceLastHighAngle++;
                else
                    ticksSinceLastHighAngle = 0;

                if (ticksSinceLastHighAngle > 300)
                {
                    Logger.Log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!increase limiter");
                    _throttleLimitMultiplier /= 1.05f;
                    ticksSinceLastHighAngle = 0;
                    // Console.WriteLine("Throttle Limit Multiplier lowered: " + _throttleLimitMultiplier);
                }
            }
           
            // we speed up in the end of the pieces (after half has passed) if situation seems to be nicely under control
            Piece nextPiece = race.track.GetNextPiece(playerCar);
            double currentPieceLength = currentPiece.GetLengthOnLane(playerCar.piecePosition.lane.endLaneIndex);
            
                if ((currentPieceLength - playerCar.piecePosition.inPieceDistance) / currentPieceLength < 0.5) // if we are on last half of the edge
                    if ( Math.Abs(playerCar.angle) < 50.0 && (angleDelta< 2.0  || angleBiggerThanOnLastUpdate == false ))
                    {
                        Logger.Log("___ corner end speed up!");
                        throttle = 1.0;
                    }
            
            
            

            lastAngle = angle;
           Logger.Log("----");
            Logger.Log("throttle: " + throttle);
            Logger.Log("velocity: " + currentVelocity);
            Logger.Log("car angle: " + carCollection.GetPlayerCar().angle);
            Logger.Log("angle delta: " + angleDelta);
            Logger.Log("angle delta norma: "+ angleDeltaNormal);
            Logger.Log("angle normal: "+ currentPiece.AngleNormal);
            Logger.Log("throttle limit multiplier: " + _throttleLimitMultiplier);
            return throttle;
        }




        
        public void RestartRace()
        {
            this.lastAngle = 0;
            this.lastAngleDelta = 0;
            this.ticksSinceLastHighAngle = 0;
        }

        
    }


}
