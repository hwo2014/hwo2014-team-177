﻿
namespace racing_bot
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class ConstantThrottleCalculator : IThrottleCalculator
    {
        private double Throttle { get; set; }

        public ConstantThrottleCalculator(double throttle)
        {
            Throttle = throttle;
        }

        public double CalculateThrottle(CarCollection carCollection, Piece currentPiece, Race race, double currentVelocity, bool insideSpeedLimit)
        {
            return Throttle;
        }



        #region IThrottleCalculator Members


        public void RestartRace()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
