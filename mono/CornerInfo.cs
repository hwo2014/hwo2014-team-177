﻿
namespace racing_bot
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Info for single corner
    /// </summary>
    public class CornerInfo
    {
        public CornerInfo(int startIndex, int endIndex, int numLanes, Track track)
        {
            StartIndex = startIndex;
            EndPieceIndex = endIndex;
            NumLanes = numLanes;
            CalculateValues(track);
        }

        private void CalculateValues(Track track)
        {
            LengthOnLane = new double[NumLanes];
            RadiusOnLane = new double[NumLanes];
        }

        public int StartIndex { get; private set; }
        public int EndPieceIndex { get; private set; }
        public int NumLanes { get; private set; }


        public double[] LengthOnLane { get; private set; }
        public double[] RadiusOnLane { get; private set; }
    }
}
