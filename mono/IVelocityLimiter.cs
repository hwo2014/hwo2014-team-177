﻿using System;
namespace racing_bot
{
    public interface IVelocityLimiter
    {
        void Calculate(Race currentRace, Car playerCar, double currentVelocity);
        double LimitedVelocity { get; set; }
        void RestartRace();
    }
}
