﻿
namespace racing_bot
{
    using System;
    using System.Collections.Generic;
    using System.Collections;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Collection of all cars in race
    /// </summary>
    public class CarCollection
    {
        Dictionary<string, Car> _carDictionary;

        public string PlayerCarName
        {
            get;
            set;
        }

        public CarCollection()
        {
            _carDictionary = new Dictionary<string, Car>();
        }

        public void UpdateCar(Car car)
        {
            if (_carDictionary.ContainsKey(car.id.name))
            {
                _carDictionary[car.id.name] = car;
            }
            else
            {
                _carDictionary.Add(car.id.name, car);   
            }
        }

        public Car GetPlayerCar()
        {
            return _carDictionary[PlayerCarName];
        }
        
    }
}
