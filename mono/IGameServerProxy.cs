﻿using System;
namespace racing_bot
{
    public interface IGameServerProxy
    {
        void SetThrottle(double throttle, int gameTick);
        void SwitchLane(string direction, int gameTick);
        void ActivateTurbo(string turboText, int gameTick);
    }
}
