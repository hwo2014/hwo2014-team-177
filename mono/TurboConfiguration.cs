﻿// -----------------------------------------------------------------------
// <copyright file="TurboConfiguration.cs" company="Tieto">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace racing_bot
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class TurboConfiguration
    {
        public double turboDurationMilliseconds;
        public int turboDurationTicks;
        public double turboFactor;
    }
}
